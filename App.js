// import React from "react";
// import { StyleSheet, Text, View, TextInput, Button } from "react-native";
// import { NavigationContainer } from "@react-navigation/native";

// import firebase from "./app/config/firebase";
// import AuthStackScreen from "./app/navigation/AuthNavigation";

// const AuthContext = createContext({});

// // driver data (youe should get it from text inputs)
// const newDriverData = {
//   name: "ِAbdulrahman Ali",
//   email: "9955acccxffsc@gmail.com",
//   password: "05016464",
//   phone: 966595486594,
//   lat: "0.01",
//   lng: "0.22",
//   schoolId: "f",
//   driverId: ""
// };

// // creating reference to driver collection in FireStore
// const driverRef = firebase.firestore().collection("drivers");

// const signUpDriver = () => {
//   // creating and logging in a new user then taking his is and adding him to the firestore!!!
//   firebase
//     .auth()
//     .createUserWithEmailAndPassword(newDriverData.email, newDriverData.password)
//     .then(userCred => {
//       // getting back user crediedntals and using it to get id
//       console.log("new user id", userCred.user.uid);
//       // passing id to create function to add the driver to the firetore
//       // this should be done to all types of users in my system
//       createDriverInfo(userCred.user.uid);
//     })
//     .catch(function(error) {
//       console.error("Error adding document: ", error.message);
//     });
// };

// // storing driver data to firestore using the uid returned from SIGN UP
// const createDriverInfo = props => {
//   driverRef
//     .doc(props)
//     .set({
//       name: newDriverData.name,
//       phone: newDriverData.phone,
//       lat: newDriverData.lat,
//       lng: newDriverData.lng,
//       schoolId: newDriverData.schoolId
//     })
//     .then(function() {
//       console.log("new user is added to firestore with id = " + props);
//     })
//     .catch(function(error) {
//       console.error("Error adding document: ", error);
//     });
// };
// // getting the user's id from auth
// const getTheID = () => {
//   if (firebase.auth().currentUser !== null)
//     console.log("user id: " + firebase.auth().currentUser.email);
// };
// // reading data functions ---------------------------
// const readData = () => {
//   // var docRef = db.collection("schools").where("num", "==", 1);

//   // docRef
//   //   .get()
//   //   .then(function(doc) {
//   //     if (doc.exists) {
//   //       console.log("Document data:", doc.data());
//   //     } else {
//   //       // doc.data() will be undefined in this case
//   //       console.log("No such document!");
//   //     }
//   //   })
//   //   .catch(function(error) {
//   //     console.log("Error getting document:", error);
//   //   });

//   // سحب داتا من كولكشن مع شرط ( استخدمها لما تسحب الطلاب المربوطين مع السواق )
//   // also you can use it with student's history
//   db.collection("cities")
//     .where("state", "==", "CA")
//     .get()
//     .then(function(querySnapshot) {
//       querySnapshot.forEach(function(doc) {
//         // doc.data() is never undefined for query doc snapshots
//         console.log(doc.id, " => ", doc.data());
//       });
//     })
//     .catch(function(error) {
//       console.log("Error getting documents: ", error);
//     });
// };
// const App = () => {
//   return (
//     <NavigationContainer>
//       <AuthStackScreen />
//     </NavigationContainer>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: "#fff",
//     alignItems: "center",
//     justifyContent: "center"
//   }
// });

// export default App;

import React from "react";
import { NavigationContainer } from "@react-navigation/native";

// to make the app only LTR "ENGLISH SUPPORTED"
import { I18nManager } from "react-native";
I18nManager.allowRTL(false);

// to dismiss keyboard by clicking anywhere

// ignore the android weird warning
console.ignoredYellowBox = ["Setting a timer"];

import AuthNavigator from "./app/navigation/AuthNavigation";

const App = () => {
  return (
    <NavigationContainer>
      <AuthNavigator />
    </NavigationContainer>
  );
};

// TODO: Check if this ruins the background location reads

// const _setTimeout = global.setTimeout;
// const _clearTimeout = global.clearTimeout;
// const MAX_TIMER_DURATION_MS = 60 * 1000;
// if (Platform.OS === "android") {
//   // Work around issue `Setting a timer for long time`
//   // see: https://github.com/firebase/firebase-js-sdk/issues/97
//   const timerFix = {};
//   const runTask = (id, fn, ttl, args) => {
//     const waitingTime = ttl - Date.now();
//     if (waitingTime <= 1) {
//       InteractionManager.runAfterInteractions(() => {
//         if (!timerFix[id]) {
//           return;
//         }
//         delete timerFix[id];
//         fn(...args);
//       });
//       return;
//     }

//     const afterTime = Math.min(waitingTime, MAX_TIMER_DURATION_MS);
//     timerFix[id] = _setTimeout(() => runTask(id, fn, ttl, args), afterTime);
//   };

//   global.setTimeout = (fn, time, ...args) => {
//     if (MAX_TIMER_DURATION_MS < time) {
//       const ttl = Date.now() + time;
//       const id = "_lt_" + Object.keys(timerFix).length;
//       runTask(id, fn, ttl, args);
//       return id;
//     }
//     return _setTimeout(fn, time, ...args);
//   };

//   global.clearTimeout = id => {
//     if (typeof id === "string" && id.startWith("_lt_")) {
//       _clearTimeout(timerFix[id]);
//       delete timerFix[id];
//       return;
//     }
//     _clearTimeout(id);
//   };
// }

export default App;
