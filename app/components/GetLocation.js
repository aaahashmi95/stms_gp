import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  SafeAreaView,
  Dimensions
} from "react-native";
import { Button } from "react-native-elements";
import MapView, { Marker } from "react-native-maps";

export default function GetLocation({ route, navigation }) {
  const [location, setLocation] = useState(0);
  const { backTo } = route.params;
  useEffect(() => {
    setLocation({ latitude: 37.78825, longitude: -122.4324 });
  }, []);
  function changeLocation(coords) {
    setLocation({ latitude: coords.latitude, longitude: coords.longitude });
    console.log(location);
  }
  return (
    // <View style={styles.container}>
    //   <Text style={styles.title}>Welcome HERE YOU WILL GET THE LOCATION!</Text>

    //   <TouchableOpacity
    //     style={styles.button}
    //     // here you can use params to get to previous page
    //     onPress={() => navigation.navigate(route.params.backTo)}
    //   >
    //     <Text style={styles.buttonText}>Save Location</Text>
    //   </TouchableOpacity>
    // </View>

    <View style={styles.container}>
      <MapView
        style={styles.mapStyle}
        initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421
        }}
        onPress={i => changeLocation(i.nativeEvent.coordinate)}
      >
        <Marker
          coordinate={{
            latitude: location.latitude,
            longitude: location.longitude
          }}
          title="LOL"
          description="bithc"
        />
      </MapView>

      <View
        style={styles.buttonsContainer}
        containerViewStyle={{ width: "100%", marginLeft: 0 }}
      >
        <TouchableOpacity
          style={styles.mapButton}
          onPress={() => navigation.navigate(backTo, { location: location })}
        >
          <Text style={styles.mapText}>Select Location</Text>
        </TouchableOpacity>
      </View>
      <SafeAreaView />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    borderColor: "transparent"
  },
  mapStyle: {
    flex: 1,
    borderColor: "transparent"
  },
  button: {
    position: "absolute",
    alignSelf: "flex-end"
  },
  buttonsContainer: {},
  mapButton: {
    alignItems: "center",
    borderColor: "transparent",

    height: 70,
    justifyContent: "center",
    backgroundColor: "#0acad4",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: -5
    },
    shadowOpacity: 0.09,
    shadowRadius: 2.22,

    elevation: 10
  },
  mapText: {
    color: "white",
    fontSize: 18,
    fontWeight: "800"
  }
});
