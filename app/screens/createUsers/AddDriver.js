import React, { useEffect, useState, useContext } from "react";
import { StyleSheet, View, TouchableOpacity, AsyncStorage } from "react-native";
import firebase from "../../config/firebase";
import { Input, Text } from "react-native-elements";

// passing navigation as a prop so i can use a button to navigate to other screens
// i moved to home page which is the one i came from so it سوى عملية رجوع back to it
// use it in location picking screen

const AddDriver = ({ route, navigation }) => {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [name, setName] = useState();
  const [phone, setPhone] = useState();

  // to use its id to STORE DATA in FIRESTORE
  const db = firebase.firestore();
  const driversColRef = db.collection("drivers");

  function createDriverAccount() {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(driver => {
        createDriverDoc(driver.user.uid);
      })
      .catch(function(error) {
        console.error("Error adding document: ", error.message);
      });
  }

  function printMyID() {
    let userId;
    AsyncStorage.getItem("userId")
      .then(u => {
        userId = u;
      })
      .catch(() => {
        console.log("There was an erroe");
      });
  }

  function createDriverDoc(driverId) {
    driversColRef
      .doc(driverId)
      .set({
        name: name,
        phone: phone,
        onTrip: false
      })
      .then(function() {
        console.log("new user is added to firestore with id =" + driverId);
      })
      .catch(function(error) {
        console.error("Error adding document: ", error);
      });
  }

  return (
    <View style={styles.container}>
      <View>
        <Input
          autoCompleteType="email"
          inputStyle={styles.input}
          inputContainerStyle={styles.inputContainer}
          placeholder="E-Mail"
          onChangeText={text => setEmail(text)}
        />
        <Input
          autoCompleteType="password"
          secureTextEntry={true}
          inputStyle={styles.input}
          inputContainerStyle={styles.inputContainer}
          placeholder="Password"
          onChangeText={text => setPassword(text)}
        />
        <Input
          autoCompleteType="name"
          inputStyle={styles.input}
          inputContainerStyle={styles.inputContainer}
          placeholder="School's Name"
          onChangeText={text => setName(text)}
        />
        <Input
          autoCompleteType="tel"
          inputStyle={styles.input}
          inputContainerStyle={styles.inputContainer}
          placeholder="Phone ex. 055123456"
          onChangeText={text => setPhone(text)}
        />
      </View>

      <TouchableOpacity style={styles.button} onPress={() => printMyID()}>
        <Text style={styles.buttonTitle}>Create New Driver</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    justifyContent: "flex-start",
    padding: 20
  },
  title: {
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 30,
    fontSize: 30,
    fontWeight: "800",
    color: "#112340"
  },
  button: {
    borderRadius: 8,
    alignItems: "center",
    backgroundColor: "#0acad4",
    padding: 10,
    height: 45,
    justifyContent: "center",
    marginHorizontal: 10,
    marginVertical: 30
  },
  inputTouchable: {
    borderRadius: 8,
    backgroundColor: "#edeef2",
    padding: 10,
    height: 45,
    justifyContent: "center",
    marginHorizontal: 10,
    marginTop: 10
  },

  buttonTitle: {
    color: "#ffffff",
    fontSize: 16,
    fontWeight: "500"
  },
  buttonText: {
    color: "#0066CB",
    fontSize: 14,
    fontWeight: "800"
  },
  inputContainer: {
    borderColor: "transparent",
    borderRadius: 8,
    backgroundColor: "#edeef2",
    marginTop: 10
  },
  text: {
    color: "#757b8e"
  },
  input: {
    paddingLeft: 10,
    fontSize: 14
  }
});
export default AddDriver;

// src/screens/Detail.js
