import React, { useEffect, useState, useContext } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import firebase from "../../config/firebase";
import { Input, Text } from "react-native-elements";

// passing navigation as a prop so i can use a button to navigate to other screens
// i moved to home page which is the one i came from so it سوى عملية رجوع back to it
// use it in location picking screen

const SignUp = ({ route, navigation }) => {
  const [locationText, setLocationText] = useState("Location");
  const [location, setLocation] = useState(null);

  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [name, setName] = useState();
  const [phone, setPhone] = useState();

  // to use its id to STORE DATA in FIRESTORE
  const db = firebase.firestore();

  const schoolsColRef = db.collection("schools");

  React.useEffect(() => {
    if (route.params?.location) {
      setLocationText(
        `(${route.params?.location.latitude}),(${route.params?.location.longitude}) `
      );
      setLocation(route.params?.location);
    }
  }, [route.params?.location]);

  async function signUp() {
    try {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(school => {
          // getting back user crediedntals and using it to get id
          // passing id to create function to add the driver to the firetore
          // this should be done to all types of users in my system
          createSchoolDoc(school.user.uid);
          alert(`Welcome ${name}! Your School is now registered..`);
        })
        .catch(function(error) {
          console.error("Error adding document: ", error.message);
        });
    } catch (e) {
      console.error(e);
    }
  }

  function createSchoolDoc(schoolId) {
    schoolsColRef
      .doc(schoolId)
      .set({
        location: new firebase.firestore.GeoPoint(
          location.latitude,
          location.longitude
        ),
        name: name,
        phone: phone
      })
      .then(function() {
        console.log("new user is added to firestore with id =" + schoolId);
      })
      .catch(function(error) {
        console.error("Error adding document: ", error);
      });
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Sign Up</Text>

      <Input
        autoCompleteType="email"
        inputStyle={styles.input}
        inputContainerStyle={styles.inputContainer}
        placeholder="E-Mail"
        onChangeText={text => setEmail(text)}
      />
      <Input
        autoCompleteType="password"
        secureTextEntry={true}
        inputStyle={styles.input}
        inputContainerStyle={styles.inputContainer}
        placeholder="Password"
        onChangeText={text => setPassword(text)}
      />
      <Input
        autoCompleteType="name"
        inputStyle={styles.input}
        inputContainerStyle={styles.inputContainer}
        placeholder="School's Name"
        onChangeText={text => setName(text)}
      />
      <Input
        autoCompleteType="tel"
        inputStyle={styles.input}
        inputContainerStyle={styles.inputContainer}
        placeholder="Phone ex. 055123456"
        onChangeText={text => setPhone(text)}
      />
      <TouchableOpacity
        style={styles.inputTouchable}
        onPress={() => navigation.navigate("Location", { backTo: "AddDriver" })}
      >
        <Text style={styles.input}>{locationText}</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button} onPress={() => signUp()}>
        <Text style={styles.buttonTitle}>Sign Up</Text>
      </TouchableOpacity>

      <TouchableOpacity style={{}} onPress={() => navigation.goBack()}>
        <View style={{ flexDirection: "row", justifyContent: "center" }}>
          <Text style={styles.text}>Already registered ? </Text>
          <Text style={styles.buttonText}>Sign In here</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    justifyContent: "center",
    padding: 20
  },
  title: {
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 30,
    fontSize: 30,
    fontWeight: "800",
    color: "#112340"
  },
  button: {
    borderRadius: 8,
    alignItems: "center",
    backgroundColor: "#0acad4",
    padding: 10,
    height: 45,
    justifyContent: "center",
    marginHorizontal: 10,
    marginVertical: 30
  },
  inputTouchable: {
    borderRadius: 8,
    backgroundColor: "#edeef2",
    padding: 10,
    height: 45,
    justifyContent: "center",
    marginHorizontal: 10,
    marginTop: 10
  },

  buttonTitle: {
    color: "#ffffff",
    fontSize: 16,
    fontWeight: "500"
  },
  buttonText: {
    color: "#0066CB",
    fontSize: 14,
    fontWeight: "800"
  },
  inputContainer: {
    borderColor: "transparent",
    borderRadius: 8,
    backgroundColor: "#edeef2",
    marginTop: 10
  },
  text: {
    color: "#757b8e"
  },
  input: {
    paddingLeft: 10,
    fontSize: 14
  }
});
export default SignUp;

// src/screens/Detail.js
