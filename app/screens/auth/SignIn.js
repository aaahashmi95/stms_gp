import React, { useState, useContext } from "react";
import {
  Alert,
  StyleSheet,
  View,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import firebase from "../../config/firebase";
import { Input, Text } from "react-native-elements";

// passing navigation as a prop so i can use a button to navigate to other screens
// i moved to home page which is the one i came from so it سوى عملية رجوع back to it
// use it in location picking screen

export default function SignIn({ navigation }) {
  const [email, setEmail] = useState("");
  const [pass, setPassword] = useState("");

  const db = firebase.firestore();
  async function signIn() {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, pass)
      .then(userCred => {
        // TODO: GetUser Id and Store it in asyncStorage or context
      })
      .catch(function(error) {
        errorAlert("Input Error", error.message);
      });
  }

  function errorAlert(title, msg) {
    Alert.alert(
      title,
      msg,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ],
      { cancelable: false }
    );
  }
  const saveUserId = async userId => {
    try {
      await AsyncStorage.setItem("userId", userId);
    } catch (error) {
      // Error retrieving data
      console.log(error.message);
    }
  };

  function fetchId() {
    AsyncStorage.getItem("userId")
      .then(u => {
        console.log("user id", u);
      })
      .catch(() => {
        console.log("There was an erroe");
      });
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>SMS Schools</Text>
      <Input
        autoCompleteType="email"
        inputStyle={styles.input}
        inputContainerStyle={styles.inputContainer}
        placeholder="E-Mail"
        onChangeText={text => setEmail(text)}
      />
      <Input
        autoCompleteType="password"
        secureTextEntry={true}
        inputStyle={styles.input}
        inputContainerStyle={styles.inputContainer}
        placeholder="Password"
        onChangeText={text => setPassword(text)}
      />
      <TouchableOpacity style={styles.button} onPress={() => signIn()}>
        <Text style={styles.buttonTitle}>Login</Text>
      </TouchableOpacity>

      <TouchableOpacity style={{}} onPress={() => navigation.push("SignUp")}>
        <View style={{ flexDirection: "row", justifyContent: "center" }}>
          <Text style={styles.text}>Don't have an account? </Text>
          <Text style={styles.buttonText}>Sign Up here</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    justifyContent: "center",
    padding: 20
  },
  title: {
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 30,
    fontSize: 40,
    fontWeight: "800",
    color: "#112340"
  },
  button: {
    borderRadius: 8,
    alignItems: "center",
    backgroundColor: "#0acad4",
    padding: 10,
    marginTop: 40,
    marginBottom: 30,
    height: 45,
    justifyContent: "center",
    marginHorizontal: 10
  },
  buttonTitle: {
    color: "#ffffff",
    fontSize: 16,
    fontWeight: "500"
  },
  buttonText: {
    color: "#0066CB",
    fontSize: 14,
    fontWeight: "800"
  },
  inputContainer: {
    borderColor: "transparent",
    borderRadius: 8,
    backgroundColor: "#edeef2",
    marginTop: 10
  },
  text: {
    color: "#757b8e"
  },
  input: {
    paddingLeft: 10,
    fontSize: 14
  }
});
