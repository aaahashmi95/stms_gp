import React, { useContext } from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";

export default function StudentsList({ navigation }) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Parents List here</Text>
      <TouchableOpacity
        style={styles.button}
        //TODO: here you can use params to get to NEXT page ADD STUDENT
        onPress={() => navigation.navigate("AddStudent")}
      >
        <Text style={styles.buttonText}>Add New Student! </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ffe2ff"
  },
  title: {
    marginTop: 20,
    marginBottom: 30,
    fontSize: 28,
    fontWeight: "500",
    color: "#7f78d2"
  },
  button: {
    flexDirection: "row",
    borderRadius: 30,
    marginTop: 10,
    marginBottom: 10,
    width: 160,
    height: 60,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#481380"
  },
  buttonText: {
    color: "#ffe2ff",
    fontSize: 24,
    marginRight: 5
  }
});
