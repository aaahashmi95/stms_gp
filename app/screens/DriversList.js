import React, { useContext } from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";

export default function DriversList({ navigation }) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Drivers List here</Text>
      <TouchableOpacity
        style={styles.button}
        //TODO: here you can use params to get to NEXT page
        onPress={() => navigation.navigate("AddDriver")}
      >
        <Text style={styles.buttonTitle}>Add New Driver</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    justifyContent: "flex-start",
    padding: 20
  },
  title: {
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 30,
    fontSize: 30,
    fontWeight: "800",
    color: "#112340"
  },
  button: {
    borderRadius: 8,
    alignItems: "center",
    backgroundColor: "#0acad4",
    padding: 10,
    height: 45,
    justifyContent: "center",
    marginHorizontal: 10,
    marginVertical: 30
  },
  inputTouchable: {
    borderRadius: 8,
    backgroundColor: "#edeef2",
    padding: 10,
    height: 45,
    justifyContent: "center",
    marginHorizontal: 10,
    marginTop: 10
  },

  buttonTitle: {
    color: "#ffffff",
    fontSize: 16,
    fontWeight: "500"
  },
  buttonText: {
    color: "#0066CB",
    fontSize: 14,
    fontWeight: "800"
  },
  inputContainer: {
    borderColor: "transparent",
    borderRadius: 8,
    backgroundColor: "#edeef2",
    marginTop: 10
  },
  text: {
    color: "#757b8e"
  },
  input: {
    paddingLeft: 10,
    fontSize: 14
  }
});
