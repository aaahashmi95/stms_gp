import React, { useContext, useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import { AuthContext } from "../navigation/AuthNavigation";
import firebase from "../config/firebase";

const db = firebase.firestore();
const schoolsColRef = db.collection("schools");

export default function Home({ navigation }) {
  const user = useContext(AuthContext);
  const [userId, setUserId] = useState(null);
  const [userObject, setUserObject] = useState();

  function fetchId() {
    AsyncStorage.getItem("userId")
      .then(u => {
        setUserId(u);
      })
      .catch(() => {
        console.log("There was an erroe");
      });
  }

  useEffect(() => {
    fetchId();
    if (userId != null) {
      console.log("userId is", userId);
    } else {
      fetchUserData(user);
    }
  }, [userId]);

  async function fetchUserData(user) {
    await schoolsColRef
      .doc(user.uid)
      .get()
      .then(doc => {
        if (doc.exists) {
          // setUserData(doc.data());
          // setUserObject({ schoolId: doc.id, data: doc.data() });
          // storeUserDataLocally(userObject);
          setUserObject({ schoolId: doc.id, ...doc.data() });
          AsyncStorage.setItem("userId", userObject.schoolId)
            .then(() => {
              console.log("Success");
            })
            .catch(() => {
              console.log("There was an erroe");
            });
        } else {
          console.log("No such document!");
        }
      })
      .catch(function(error) {
        console.error("Error adding document: ", error);
      });
  }
  async function logOut() {
    try {
      await firebase
        .auth()
        .signOut()
        .then(() => {
          AsyncStorage.setItem("userId", "");
          // clean up user data from asyncStorage
        });
    } catch (e) {
      console.error(e);
    }
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Welcome {}!</Text>
      <TouchableOpacity style={styles.button} onPress={logOut}>
        <Text style={styles.buttonText}>Sign out 🤷</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.toggleDrawer()}
      >
        <Text style={styles.buttonText}>Sign out 🤷</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ffe2ff"
  },
  title: {
    marginTop: 20,
    marginBottom: 30,
    fontSize: 28,
    fontWeight: "500",
    color: "#7f78d2"
  },
  button: {
    flexDirection: "row",
    borderRadius: 30,
    marginTop: 10,
    marginBottom: 10,
    width: 160,
    height: 60,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#481380"
  },
  buttonText: {
    color: "#ffe2ff",
    fontSize: 24,
    marginRight: 5
  }
});
