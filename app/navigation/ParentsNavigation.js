import React, { useState, useEffect, createContext } from "react";
import { createStackNavigator } from "@react-navigation/stack";

import ParentsList from "../screens/ParentsList";
import AddParent from "../screens/createUsers/AddParent";

const Stack = createStackNavigator();

export default function ParentsStack() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="ParentsList" component={ParentsList} />
      <Stack.Screen name="AddParent" component={AddParent} />
    </Stack.Navigator>
  );
}
