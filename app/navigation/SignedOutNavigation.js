import React, { useState, useEffect, createContext } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import SignIn from "../screens/auth/SignIn";
import SignUp from "../screens/auth/SignUp";
import GetLocation from "../components/GetLocation";

const SignedOutStack = createStackNavigator();

export default function SignedOutScreen() {
  return (
    <SignedOutStack.Navigator headerMode="none">
      <SignedOutStack.Screen name="Login" component={SignIn} />
      <SignedOutStack.Screen name="SignUp" component={SignUp} />
      <SignedOutStack.Screen name="Location" component={GetLocation} />
    </SignedOutStack.Navigator>
  );
}
