import React, { useState, useEffect, createContext } from "react";
import { createStackNavigator } from "@react-navigation/stack";

import DriversList from "../screens/DriversList";
import AddDriver from "../screens/createUsers/AddDriver";
import GetLocation from "../components/GetLocation";

const Stack = createStackNavigator();

export default function DriversStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="DriversList"
        component={DriversList}
        options={{
          title: "",
          headerStyle: {
            backgroundColor: "white",
            shadowOffset: { height: 0, width: 0 }
          }
        }}
      />
      <Stack.Screen
        name="AddDriver"
        component={AddDriver}
        options={{
          gestureEnabled: false
        }}
      />
    </Stack.Navigator>
  );
}
