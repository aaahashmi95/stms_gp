import React, { useState, useEffect, createContext } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { AsyncStorage } from "react-native";
import firebase from "./../config/firebase";

export const AuthContext = createContext(null);

import SignedOutStack from "./SignedOutNavigation";
import DrawerNavigation from "./DrawerNavigation";

const AuthStack = createStackNavigator();

export default function AuthNavigator() {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState(null);

  // Handle user state changes
  function onAuthStateChanged(result) {
    setUser(result);
    if (initializing && user == null) setInitializing(false);
  }

  useEffect(() => {
    const authSubscriber = firebase
      .auth()
      .onAuthStateChanged(onAuthStateChanged);
    // unsubscribe on unmount
    return authSubscriber;
  }, []);

  if (initializing) {
    return null;
  }

  return user ? (
    <AuthContext.Provider value={user}>
      <DrawerNavigation />
    </AuthContext.Provider>
  ) : (
    <SignedOutStack />
  );
}
