import React, { useState, useEffect, createContext } from "react";
import { createStackNavigator } from "@react-navigation/stack";

import StudentsList from "../screens/StudentsList";
import AddStudent from "../screens/createUsers/AddStudent";

const Stack = createStackNavigator();

export default function StudentsStack() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="StudentsList" component={StudentsList} />
      <Stack.Screen name="AddStudent" component={AddStudent} />
    </Stack.Navigator>
  );
}
