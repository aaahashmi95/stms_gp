import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import SchoolInfo from "../screens/auth/SignUp";
import HomeStack from "./HomeNavigation";
import StudentsStack from "./StudentsNavigation";
import DriversStack from "./DriversNavigation";
import ParentsStack from "./ParentsNavigation";

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => (
  <Drawer.Navigator initialRouteName="Home">
    <Drawer.Screen name="Home" component={HomeStack} />
    <Drawer.Screen name="Drivers" component={DriversStack} />
    <Drawer.Screen name="Students" component={StudentsStack} />
    <Drawer.Screen name="Parents" component={ParentsStack} />
  </Drawer.Navigator>
);

export default DrawerNavigator;
