import * as firebase from "firebase";
import "firebase/firestore";
const firebaseConfig = {
  apiKey: "AIzaSyAlf-kx1RCeAjY44W-MIMp5kG34P5sLLCE",
  authDomain: "students-monitoring-system.firebaseapp.com",
  databaseURL: "https://students-monitoring-system.firebaseio.com",
  projectId: "students-monitoring-system",
  storageBucket: "students-monitoring-system.appspot.com",
  messagingSenderId: "159953554583",
  appId: "1:159953554583:web:93278f66928d50e16fadd2",
  measurementId: "G-RPJ24CL8RV"
};

firebase.initializeApp(firebaseConfig);

firebase.firestore();

export default firebase;
